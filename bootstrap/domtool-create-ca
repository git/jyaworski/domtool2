#!/bin/bash
# -*- sh -*-

# Create a domtool certificate authority
# WARNING: Will not create a secure CA if it is in afs space

if [[ `whoami` != "root" && "$1" != "-force" ]]; then
    echo "This should be run as root. Use -force to force creating a CA"
    echo "as a normal user"
    exit 1
fi

# use domtool-config to extract ca path and site domain

CAPATH=`../bin/domtool-config -path cert ca`
BASE_OPENSSL_CONFIG=`../bin/domtool-config -domain`.core.ssl.conf

if [ ! -f $BASE_OPENSSL_CONFIG ]; then
    echo "You need to create $BASE_OPENSSL_CONFIG before continuing"
    exit 1
fi

cat $BASE_OPENSSL_CONFIG common.ssl.conf > domtool-openssl.conf

if [ -z "$CAPATH" ]; then
    echo "No CA path set. Domtool has not yet been built?"
    exit 1
fi

# 1. Create directory structure

mkdir -p $CAPATH
for d in crl newcerts private; do
    mkdir $CAPATH/$d
done

chmod go-rwx $CAPATH/private
echo '01' > $CAPATH/serial
touch $CAPATH/index

# 2. Generate private key

openssl req -nodes -config domtool-openssl.conf -days 1825 -x509 -newkey rsa -out $CAPATH/ca-cert.pem -outform PEM

# 3. Copy ssl configuration to ca dir

# In general, publishing the openssl config for a domain in the ca
# directory might not be the best idea, but since this is a limited
# use internal CA, it is probably not a big deal.
cp domtool-openssl.conf $CAPATH/
chmod 600 $CAPATH/domtool-openssl.conf

# Does the CA need to be readable by domtool? Issues with sudo and
# tickets, but those could be solved by creating a 700
# /tmp/domtool-ca-out/ and chowning to the actual user after for the
# copy/delete. Or maybe the ca ought to live in afs
# space... generality issues arise, probably just do option #1.
