(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 * Copyright (c) 2011,2013,2014 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Firewall rule querying/generation *)

signature FIREWALL = sig

    datatype user = User of string

    datatype fwnode = FirewallNode of string

    datatype fwrule = Client of int list * string list
		    | Server of int list * string list
		    | ProxiedServer of int list
		    | LocalServer of int list

    type firewall_rules = (user * fwnode * fwrule) list

    val query : string * string -> string list
    (* List a user's local firewall rules. *)

    val parseRules : unit -> firewall_rules
    val generateFirewallConfig : firewall_rules -> bool

    val publishConfig : unit -> bool

end
