(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* MySQL user/table management *)

structure MySQL :> MYSQL = struct

val validPasswd = CharVector.all (fn ch => Char.isAlphaNum ch
					   orelse ch = #"!"
					   orelse ch = #"."
					   orelse ch = #"-"
					   orelse ch = #"_")

fun adduser {user, passwd} =
    case passwd of
	NONE => SOME "No password given"
      | SOME passwd =>
	if validPasswd passwd then
	    Option.map (fn s => "Error executing CREATE USER script:\n" ^ s)
		       (Slave.shellOutput [Config.MySQL.adduser, " ", user, " ", passwd])
	else
	    SOME "Password contains characters besides letters, digits, and !.-_"

fun passwd {user, passwd} =
    if validPasswd passwd then
	Option.map (fn s => "Error executing SET PASSWORD script:\n" ^ s)
		   (Slave.shellOutput [Config.MySQL.passwd, " ", user, " ", passwd])
    else
	SOME "Password contains characters besides letters, digits, and !.-_"

fun createdb {user, dbname, encoding} =
    case encoding of
	SOME _ => SOME "MySQL doesn't support specifying encodings"
      | NONE =>
	Option.map (fn s => "Error executing CREATE DATABASE script:\n" ^ s)
		   (Slave.shellOutput [Config.MySQL.createdb, " ", user, " ", dbname])

fun dropdb {user, dbname} =
    Option.map (fn s => "Error executing DROP DATABASE script:\n" ^ s)
	       (Slave.shellOutput [Config.MySQL.dropdb, " ", user, " ", dbname])

fun grant {user, dbname} =
    Option.map (fn s => "Error executing GRANT script:\n" ^ s)
	       (Slave.shellOutput [Config.MySQL.grant, " ", user, " ", dbname])

val _ = Dbms.register ("mysql", {getpass = SOME Client.getpass,
				 adduser = adduser,
				 passwd = passwd,
				 createdb = createdb,
				 dropdb = dropdb,
				 grant = grant})

end
