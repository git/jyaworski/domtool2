(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 * Copyright (c) 2011,2012,2013,2014 Clinton Ebadi
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Firewall management *)

(* Contains portions from Fwtool Copyright (C) 2005  Adam Chlipala, GPL v2 or later *)

structure Firewall :> FIREWALL = struct

datatype user = User of string
		    
datatype fwnode = FirewallNode of string

datatype fwrule = Client of int list * string list
		| Server of int list * string list
		| ProxiedServer of int list
		| LocalServer of int list

type firewall_rules = (user * fwnode * fwrule) list

structure StringMap = DataStructures.StringMap

fun parseRules () =
    let
	val inf = TextIO.openIn Config.Firewall.firewallRules

	fun parsePorts ports =
	    List.mapPartial Int.fromString (String.fields (fn ch => ch = #",") ports)
	    (* Just drop bad ports for now *)
		       
	fun loop parsedRules =
	    case TextIO.inputLine inf of
		NONE => parsedRules
	      | SOME line =>
		case String.tokens Char.isSpace line of
		    node :: uname :: rest =>
		    (case rest of
			"Client" :: ports :: hosts => loop ((User uname, FirewallNode node, Client (parsePorts ports, hosts)) :: parsedRules)
		      | "Server" :: ports :: hosts => loop ((User uname, FirewallNode node, Server (parsePorts ports, hosts)) :: parsedRules)
		      | ["ProxiedServer", ports] => loop ((User uname, FirewallNode node, ProxiedServer (parsePorts ports)) :: parsedRules)
		      | ["LocalServer", ports] => loop ((User uname, FirewallNode node, LocalServer (parsePorts ports)) :: parsedRules)
		      | _ => (print "Invalid config line\n"; loop parsedRules))
		  | _ => loop parsedRules
    in
	loop []
    end

fun formatQueryRule (Client (ports, hosts)) =
    "Client " ^ String.concatWith "," (map Int.toString ports) ^ " " ^ String.concatWith " " hosts
  | formatQueryRule  (Server (ports, hosts)) =
    "Server " ^ String.concatWith "," (map Int.toString ports) ^ " " ^ String.concatWith " " hosts
  | formatQueryRule (ProxiedServer ports) =
    "ProxiedServer " ^ String.concatWith "," (map Int.toString ports)
  | formatQueryRule (LocalServer ports) =
    "LocalServer " ^ String.concatWith "," (map Int.toString ports)

fun query (node, uname) =
    (* completely broken *)
    let
	val rules = parseRules ()
    in
	map (fn (_, _, r) => formatQueryRule r)
	    (List.filter (fn (User u, FirewallNode n, _) => u = uname andalso n = node) rules)
    end

fun formatPorts ports = "(" ^ String.concatWith " " (map Int.toString ports) ^ ")"
fun formatHosts hosts = "(" ^ String.concatWith " " hosts ^ ")"

fun formatOutputRule (Client (ports, hosts)) = "dport " ^ formatPorts ports ^ (case hosts of 
										 [] => ""
									       | _ => " daddr " ^ formatHosts hosts) ^ " ACCEPT;"
  | formatOutputRule _ = ""

fun formatInputRule (Server (ports, hosts)) = "dport " ^ formatPorts ports ^ (case hosts of 
										  [] => ""
										| _ => " saddr " ^ formatHosts hosts) ^ " ACCEPT;"
  | formatInputRule _ = ""

type ferm_lines = { input_rules : (string list) DataStructures.StringMap.map,
		    output_rules : (string list) DataStructures.StringMap.map } 

fun generateNodeFermRules rules  = 
    let
	fun filter_node_rules rules =
	    List.filter (fn (uname, FirewallNode node, rule) => node = Slave.hostname () orelse case rule of 
												    ProxiedServer _ => List.exists (fn (h,_) => h = Slave.hostname ()) Config.Apache.webNodes_all
										     | _ => false)
			rules

	val inputLines = ref StringMap.empty
	val outputLines = ref StringMap.empty

	fun confLine r (User uname, line) =
	    let
		val line = "\t" ^ line ^ "\n"
		val lines = case StringMap.find (!r, uname) of
				NONE => []
			      | SOME lines => lines
	    in
		r := StringMap.insert (!r, uname, line :: lines)
	    end

	fun confLine_in (uname, rule) = confLine inputLines (uname, formatInputRule rule)
	fun confLine_out (uname, rule) = confLine outputLines (uname, formatOutputRule rule)

	fun insertConfLine (uname, ruleNode, rule) =
	    case rule of 
		Client (ports, hosts) => confLine_out (uname, rule)
	      | Server (ports, hosts) => confLine_in (uname, rule)
	      | LocalServer ports => (insertConfLine (uname, ruleNode, Client (ports, ["127.0.0.1/8"]));
				      insertConfLine (uname, ruleNode, Server (ports, ["127.0.0.1/8"])))
	      | ProxiedServer ports => if (fn FirewallNode r => r) ruleNode = Slave.hostname () then
					   (insertConfLine (uname, ruleNode, Server (ports, ["$WEBNODES"]));
					    insertConfLine (uname, ruleNode, Client (ports, [(fn FirewallNode r => r) ruleNode])))
				       else (* we are a web server *)
					   (insertConfLine (uname, ruleNode, Client (ports, [(fn FirewallNode r => r) ruleNode]));
					    insertConfLine (User "www-data", ruleNode, Client (ports, [(fn FirewallNode r => r) ruleNode])))

	val _ = map insertConfLine (filter_node_rules rules)
    in
	{ input_rules = !inputLines,
	  output_rules = !outputLines }


    end

fun generateFirewallConfig rules =
    (* rule generation must happen on the node (mandating the even
       service users be pts users would make it possible to do on the
       server, but that's not happening any time soon) *)
    let
	val users_tcp_out_conf = TextIO.openOut (Config.Firewall.firewallDir ^ "/users_tcp_out.conf")
	val users_tcp_in_conf = TextIO.openOut (Config.Firewall.firewallDir ^ "/users_tcp_in.conf")
	val user_chains_conf = TextIO.openOut (Config.Firewall.firewallDir ^ "/user_chains.conf")

	val nodeFermRules = generateNodeFermRules rules
		
	fun write_tcp_in_conf_preamble outf = 
	    TextIO.output (outf, String.concat ["@def $WEBNODES = (",
						(String.concatWith " " (List.map (fn (_, ip) => ip) 
										 (List.filter (fn (node, _) => List.exists (fn (n) => n = node) (List.map (fn (node, _) => node) (Config.Apache.webNodes_all @ Config.Apache.webNodes_admin)))
											      Config.nodeIps))),
						");\n\n"])

	fun writeUserInRules (uname, lines) = 
	    (* We can't match the user when listening; SELinux or
  	       similar would let us manage this with better
	       granularity.*)
	    let
		val _ = SysWord.toInt (Posix.ProcEnv.uidToWord (Posix.SysDB.Passwd.uid (Posix.SysDB.getpwnam uname)))
	    in
		TextIO.output (users_tcp_in_conf, "proto tcp {\n");
		TextIO.output (users_tcp_in_conf, concat lines);
		TextIO.output (users_tcp_in_conf, "\n}\n\n")
	    end handle OS.SysErr _ => print "Invalid user in firewall config, skipping.\n" (* no sense in opening ports for bad users *)		

	fun writeUserOutRules (uname, lines) =
	    let
		val uid = SysWord.toInt (Posix.ProcEnv.uidToWord (Posix.SysDB.Passwd.uid (Posix.SysDB.getpwnam uname)))
	    in
		TextIO.output (users_tcp_out_conf, "mod owner uid-owner " ^ (Int.toString uid)
						   ^ " { jump user_" ^ uname ^ "_tcp_out"
						   ^ "; DROP; }\n");

		TextIO.output (user_chains_conf, "chain user_" ^ uname ^ "_tcp_out"
						 ^ " proto tcp {\n");
		TextIO.output (user_chains_conf, concat lines);
		TextIO.output (user_chains_conf, "\n}\n\n")
	    end handle OS.SysErr _ => print "Invalid user in firewall config, skipping.\n"
	    
    in
	write_tcp_in_conf_preamble (users_tcp_in_conf);
	StringMap.appi (writeUserOutRules) (#output_rules nodeFermRules);
	StringMap.appi (writeUserInRules) (#input_rules nodeFermRules);

	TextIO.closeOut user_chains_conf;
	TextIO.closeOut users_tcp_out_conf;
	TextIO.closeOut users_tcp_in_conf;

	true
    end

fun publishConfig _ = 
    Slave.shell [Config.Firewall.reload]
end
