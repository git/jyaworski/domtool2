(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Exim MTA handling *)

structure Exim :> EXIM = struct

open Ast

val aliasesChanged = ref false
val aliasesDefaultChanged = ref false
val hostsChanged = ref false
val relayHostsChanged = ref false

val () = Slave.registerPreHandler
	     (fn () => (aliasesChanged := false;
			aliasesDefaultChanged := false;
			hostsChanged := false))

val () = Slave.registerFileHandler (fn fs =>
				       let
					   val spl = OS.Path.splitDirFile (#file fs)
				       in
					   if #file spl = "aliases.base" then
					       aliasesChanged := true
					   else if #file spl = "aliases.default" then
					       aliasesDefaultChanged := true
					   else if #file spl = "mail.handle" then
					       hostsChanged := true
					   else if #file spl = "mail.relay" then
					       relayHostsChanged := true
					   else
					       ()
				       end)

val () = Slave.registerPostHandler
	 (fn () =>
	     (if !aliasesChanged then
		  Slave.concatTo (fn s => s = "aliases.base") Config.Exim.aliases
	      else
		  ();
	      if !aliasesDefaultChanged then
		  Slave.concatTo (fn s => s = "aliases.default") Config.Exim.aliasesDefault
	      else
		  ();
	      if !hostsChanged then
		  Slave.enumerateTo (fn s => s = "mail.handle") ":" Config.Exim.handleDomains
	      else
		  ();
	      if !relayHostsChanged then
		  Slave.enumerateTo (fn s => s = "mail.relay") ":" Config.Exim.relayDomains
	      else
		  ();
	      if !aliasesChanged orelse !aliasesDefaultChanged
		 orelse !hostsChanged orelse !relayHostsChanged then
		  Slave.shellF ([Config.Exim.reload],
				fn cl => "Error reloading exim with " ^ cl)
	      else
		  ()))


val () = Env.actionV_none "handleMail"
	 (fn env =>
	     let
		 val nodes = Env.env (Env.list Env.string) (env, "MailNodes")
	     in
		 app (fn node => #close
				     (Domain.domainsFile {node = node,
							  name = "mail.handle"}) ()) nodes
	     end)

val () = Env.actionV_none "relayMail"
	 (fn env =>
	     let
		 val nodes = Env.env (Env.list Env.string) (env, "MailNodes")
	     in
		 app (fn node => #close
				     (Domain.domainsFile {node = node,
							  name = "mail.relay"}) ()) nodes
	     end)

val () = Domain.registerDescriber (Domain.considerAll
				   [Domain.Filename {filename = "aliases.base",
						     heading = "E-mail aliases:",
						     showEmpty = false},
				    Domain.Filename {filename = "aliases.default",
						     heading = "Default e-mail alias:",
						     showEmpty = false},
				    Domain.Filename {filename = "mail.handle",
						     heading = "E-mail handling is on.",
						     showEmpty = true},
				    Domain.Filename {filename = "mail.relay",
						     heading = "E-mail relaying is on.",
						     showEmpty = true}])

end
