(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Pretty-printing Domtool configuration file ASTs *)

signature PRINTFN_INPUT = sig

    structure PD : PP_DESC

    val keyword : string -> PD.pp_desc
    val punct : string -> PD.pp_desc
    val field : string -> PD.pp_desc
    val lit : string -> PD.pp_desc
    val ident : string -> PD.pp_desc

    val context : string -> PD.pp_desc
    val typ : string -> PD.pp_desc
    val exp : string -> PD.pp_desc

    val anchor : string * PD.pp_desc -> PD.pp_desc
    val link : string * PD.pp_desc -> PD.pp_desc

    type rendering
    val openStream : unit -> PD.PPS.stream
    val closeStream : PD.PPS.stream -> rendering

end

signature PRINTFN_OUTPUT = sig

    structure PD : PP_DESC
    type rendering

    val p_pred : Ast.pred -> PD.pp_desc
    val p_typ : Ast.typ -> PD.pp_desc
    val p_exp : Ast.exp -> PD.pp_desc
    val p_decl : Ast.decl' -> PD.pp_desc
    val p_decl_fref : Ast.decl' -> PD.pp_desc

    val output : PD.pp_desc -> rendering
    val preface : string * PD.pp_desc -> rendering

end
