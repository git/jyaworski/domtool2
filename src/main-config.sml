(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2014 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Configuration Value Queries *)

(* Not entirely clear what belongs here and what belongs in
   domtool-admin.

   Proposed dividing line: whatever can be deduced from the static
   configuration can be queried with domtool-config, anything that
   requires acessing the daemons goes through domtool-admin. 
*)

fun println x = (print x; print "\n")
fun printerr x = (TextIO.output (TextIO.stdErr, x); TextIO.flushOut TextIO.stdErr)
fun die reason = (printerr reason; printerr "\n"; OS.Process.exit OS.Process.failure)

val _ =
    (case CommandLine.arguments () of 
	("-path" :: path) => (case path of
				  ["shared-root"] => println Config.sharedRoot
				| ["local-root"] => println Config.localRoot
				| ["install-prefix"] => println Config.installPrefix
				| ["cert", path] => (case path of 
							 "ca" => println Config.caDir
						       | "certs" => println Config.certDir
						       | "keys" => println Config.keyDir
						       | "truststore" => println Config.trustStore
						       | _ => die "Invalid cert path type")
				| ["vmaildb"] => println Config.Vmail.userDatabase
				| _ => die "Invalid path type")
      | ["-nodes"] => (app (fn (n, _) => (print n; print " ")) Config.nodeIps;
		       print "\n")
      | ["-domain"] => println Config.defaultDomain
      | _ => die "Invalid command-line arguments")
