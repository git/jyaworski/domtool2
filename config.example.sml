structure Config :> CONFIG = struct

open ConfigDefault

val myNode = "doomfaring"

val nodeIps = [(myNode, "127.0.0.1")]
val defaultDomain = "home.unknownlamer.org" (* $node.$defaultDomain must resolve *)
val dispatcherName = myNode
val caDir = "/home/clinton/domtool/ca" (* Replace with your homedir *)

val dnsNodes_all = [myNode]
val dnsNodes_admin = []

val mailNodes_all = [myNode]
val mailNodes_admin = []

structure Apache : APACHE_CONFIG = struct
open Apache
open ConfigTypes

val webNodes_all = [(myNode, {version = APACHE_2, auth = MOD_WAKLOG})]
val webNodes_admin = []
val webNodes_default = [myNode]

end

structure Bind : BIND_CONFIG = struct
open Bind
val masterNode = myNode
val slaveNodes = []
end

structure Dbms : DBMS_CONFIG = struct
open Dbms
val dbmsNode = myNode
end

structure Firewall : FIREWALL_CONFIG = struct
open Firewall
val firewallNodes = [myNode]
end

structure Mailman : MAILMAN_CONFIG = struct
open Mailman
val node = myNode
end

end
